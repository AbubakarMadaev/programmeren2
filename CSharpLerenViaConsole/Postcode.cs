﻿﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

// namespace declareren in pascalnotatie
namespace CSharpLerenViaConsole
{
    class Postcode
    {
        //velden maken we private = camelcasenotatie
        private string code;

        //properties maken we public = pascalnotatie
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        private string plaats;

        public string Plaats
        {
            get { return plaats; }
            set { plaats = value; }
        }

        private string provincie;

        public string Provincie
        {
            get { return provincie; }
            set { provincie = value; }
        }

        private string localite;

        public string Localite
        {
            get { return localite; }
            set { localite = value; }
        }

        private string province;

        public string Province
        {
            get { return province; }
            set { province = value; }
        }
        
        public string ReadPostcodersFromCSVFile()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = "Data//postcodes.csv";
            // lees het bestand in vanop de harde schijf
            bestand.Lees();
            return bestand.Text;
        }

        public void GetPostcodeList()
        {
            string text = ReadPostcodersFromCSVFile();
            string[] postcodes = text.Split('\n');
        }

        public string SerializeObjectToCsv(List<Postcode> list, string separator)
        {
            string fileName = @"Data/Postcodes2.csv";
            string message;
            try
            {
                TextWriter writer = new StreamWriter(fileName);
                foreach (Postcode item in list)
                {
                    // One of the most versatile and useful additions to the C# language in version 6
                    // is the null conditional operator ?.           
                    writer.WriteLine("{0}{5}{1}{5}{2}{5}{3}{5}{4}",
                        item?.Code,
                        item?.Plaats,
                        item?.Provincie,
                        item?.Localite,
                        item?.Province,
                        separator);
                }
                writer.Close();
                message = $"Het bestand met de naam {fileName} is gemaakt!";
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                message = $"Kan het bestand met de naam {fileName} niet maken.\nFoutmelding {e.Message}.";
            }
      
            return message;
        }
    }



}

