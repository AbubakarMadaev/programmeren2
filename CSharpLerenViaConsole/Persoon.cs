﻿﻿﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Blog
{
    class Persoon
    {
        //public string voornaam;
        //public string familienaam;
        //private int leeftijd;

        //public int Leeftijd
        //{
        //    get { return leeftijd; }
        //    set
        //    {
        //        if (value >= 0 && value <= 120)
        //        {
        //            leeftijd = value;
        //        }
        //        else
        //        {
        //            Console.WriteLine($"{value} is geen geldige leeftijd! Leeftijd tussen 0 en 120");
        //        }
        //    }
        //}

        //public string ShowInfo()
        //{
        //    return $"Voornaam: {voornaam}\nFamilienaam: {familienaam}\n leeftijd: {leeftijd}";
        //}

        //waarom private field? om uw data te beschermen zodat deze niet kan gewijzigd worden
        private string voornaam;

        //properties maken we public = pascalnotatie
        public string Voornaam
        {
            get { return voornaam; }
            set { voornaam = value; }
        }

        private string familienaam;

        public string Familienaam
        {
            get { return familienaam; }
            set { familienaam = value; }
        }

        private string straat;

        public string Straat
        {
            get { return straat; }
            set { straat = value; }
        }

        private string postcode;

        public string Postcode
        {
            get { return postcode; }
            set { postcode = value; }
        }

        private string stad;

        public string Stad
        {
            get { return stad; }
            set { stad = value; }
        }

        public string SerializeObjectToCsv(List<Persoon> list, string separator)
        {
            string fileName = @"Data/Personen.csv";
            string message;
            try
            {
                TextWriter writer = new StreamWriter(fileName);
                foreach (Persoon item in list)
                {
                    // One of the most versatile and useful additions to the C# language in version 6
                    // is the null conditional operator ?.           
                    writer.WriteLine("{0}{5}{1}{5}{2}{5}{3}{5}{4}",
                        item?.Voornaam,
                        item?.Familienaam,
                        item?.Straat,
                        item?.Postcode,
                        item?.Stad,
                        separator);
                }
                writer.Close();
                message = $"Het bestand met de naam {fileName} is gemaakt!";
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                message = $"Kan het bestand met de naam {fileName} niet maken.\nFoutmelding {e.Message}.";
            }

            return message;
        }

        //gaat bestand inlezen als tekstbestand
        public string ReadFromCSVFile()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = "Data//Personen.csv";
            // lees het bestand in vanop de harde schijf
            bestand.Lees();
            return bestand.Text;
        }

        //gaat het bestand inlezen als array
        public string[] GetArray()
        {
            string text = ReadFromCSVFile();
            string[] personen = text.Split('\n');
            return personen;
        }

        public List<Persoon> GetList(string[] personen)
        {
            List<Persoon> personenLijst = new List<Persoon>();
            foreach (string item in personen)
            {
                string[] persoonsgegevens = item.Split(';');
                Persoon persoon = new Persoon();
                persoon.Voornaam = item[0].ToString();
                persoon.Familienaam = item[1].ToString();
                persoon.Straat = item[2].ToString();
                persoon.Postcode = item[3].ToString();
                persoon.Stad = item[4].ToString();
                personenLijst.Add(persoon);
            }
            return personenLijst;

        }
    }
}
